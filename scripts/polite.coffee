# Description:
#   Carrotbot is a nice bot
#
# Commands:
#   thanks hubot or thank you hubot

module.exports = (bot) ->
  bot.hear /(thanks carrotbot|thank you carrotbot)/i, (msg) ->
    msg.send "you're most welcome!"
