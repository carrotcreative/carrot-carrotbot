# Carrotbot

A little bot for our [slack](http://slack.com) chatroom.

### Getting Started

- Clone this repo down
- Run `npm install`

### Testing Locally

```
% ./bin/hubot
> hubot help
```

### Adding Scripts

Just check out the `/scripts` folder for a few examples. For additional help, consult hubot's [scripting guide](https://github.com/github/hubot/blob/master/docs/scripting.md).

### Deploying


This bot was designed to be deployed to heroku. If you are pulling this repo, it's likely that it has already been deployed and you can just ask to be added to the heroku project and push when ready. But if you'd like to set this thing up for the first time, instructions are below:

- Install heroku toolbelt if you haven't already.

    ```
    % heroku create carrotbot
    % heroku addons:add redistogo:nano
    ```

    or optionally

    ```
    % git remote add heroku git@heroku.com:carrotbot.git
    ```

- Activate the Hubot service on your "Team Services" page inside Slack.
- Add the config variables. For example:

    ```
    % heroku config:add HEROKU_URL=http://carrotbot.herokuapp.com
    % heroku config:add HUBOT_SLACK_TOKEN=Nx0zOcBNxfdOdueQzmc8BjkE
    % heroku config:add HUBOT_SLACK_TEAM=carrot
    % heroku config:add HUBOT_SLACK_BOTNAME=carrotbot
    ```

- Deploy and start the bot:

    ```
    % git push heroku master
    % heroku ps:scale web=1
    ```

### Running Tests

Tests? Psh, we don't need something serious like that for our chat bot, come on. Just test your functionality locally before you push to heroku.
